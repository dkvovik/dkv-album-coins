import Vue from 'vue'
import Vuex from 'vuex'

import shared from './modules/shared'
import user from './modules/user'
import albums from './modules/albums'
import coins from './modules/coins'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    shared,
    user,
    albums,
    coins
  }
})
