import fb from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'

class Coin {
  constructor (userId, nominal, year, country, diameter = null, thickness = null, mint = '', weight = null, actualWeight = null, metal = null, album, quality, description, imageObverseSrc = '', imageReverseSrc = '', imageHerdSrc = '') {
    this.userId = userId
    this.nominal = nominal
    this.year = year
    this.country = country
    this.diameter = diameter
    this.thickness = thickness
    this.mint = mint
    this.weight = weight
    this.actualWeight = actualWeight
    this.metal = metal
    this.album = album
    this.quality = quality
    this.description = description
    this.imageObverseSrc = imageObverseSrc
    this.imageReverseSrc = imageReverseSrc
    this.imageHerdSrc = imageHerdSrc
  }
}

export default {
  state: {
    coins: []
  },
  mutations: {
    addCoin (state, payload) {
      state.coins.push(payload)
    },
    fetchCoins (state, payload) {
      state.coins = payload
    }
  },
  actions: {
    async addCoin ({commit, getters}, payload) {
      commit('clearError')
      commit('setLocalLoading', true)

      const imageObverse = payload.imageObverse
      const imageReverse = payload.imageReverse
      const imageHerd = payload.imageHerd

      try {
        const coin = new Coin(
          getters.getUser.id,
          payload.nominal,
          payload.year,
          payload.country,
          payload.diameter,
          payload.thickness,
          payload.mint,
          payload.weight,
          payload.actualWeight,
          payload.metal,
          payload.album,
          payload.quality,
          payload.description
        )

        let newCoin = await fb.firestore().collection('users').doc(`${getters.getUser.id}`).collection(`coins`).add(JSON.parse(JSON.stringify(coin)))

        const imageObverseExt = imageObverse.name.slice(imageObverse.name.lastIndexOf('.'))
        await fb.storage().ref().child(`${coin.userId}/coins/${newCoin.id}/${newCoin.id}_observe${imageObverseExt}`).put(imageObverse)
          .then(async snapshot => {
            coin.imageObverseSrc = await snapshot.ref.getDownloadURL()
          })

        const imageReverseExt = imageReverse.name.slice(imageReverse.name.lastIndexOf('.'))
        await fb.storage().ref().child(`${coin.userId}/coins/${newCoin.id}/${newCoin.id}_reverse${imageReverseExt}`).put(imageReverse)
          .then(async snapshot => {
            coin.imageReverseSrc = await snapshot.ref.getDownloadURL()
          })

        const imageHerdExt = imageHerd.name.slice(imageHerd.name.lastIndexOf('.'))
        await fb.storage().ref().child(`${coin.userId}/coins/${newCoin.id}/${newCoin.id}_herd${imageHerdExt}`).put(imageHerd)
          .then(async snapshot => {
            coin.imageHerdSrc = await snapshot.ref.getDownloadURL()
          })

        newCoin = await fb.firestore().collection('users').doc(`${getters.getUser.id}`).collection(`coins`).doc(newCoin.id).update({
          imageObverseSrc: coin.imageObverseSrc,
          imageReverseSrc: coin.imageReverseSrc,
          imageHerdSrc: coin.imageHerdSrc
        })

        commit('addCoin', newCoin)
        commit('setLocalLoading', false)
      } catch (error) {
        commit('setLocalLoading', false)
        commit('setError', error.message)
        throw error
      }
    },
    async fetchCoins ({commit, getters}) {
      commit('setLocalLoading', true)
      commit('clearError')

      let allCoins = []
      try {
        const fetchCoinsFromServer =
          await fb.firestore().collection('users').doc(`${getters.getUser.id}`).collection('coins').get()
        fetchCoinsFromServer.forEach((doc) => {
          let album = doc.data()
          album['id'] = doc.id
          allCoins.push(album)
        })
        commit('fetchCoins', allCoins)
        commit('setLocalLoading', false)
      } catch (error) {
        commit('setLocalLoading', false)
        commit('setError', error.message)
        throw error
      }
    }
  },
  getters: {
    getCoins (state) {
      return state.coins
    }
  }
}
