export default {
  state: {
    loading: false,
    localLoading: false,
    error: null
  },
  mutations: {
    setLoading (state, payload) {
      state.loading = payload
    },
    setLocalLoading (state, payload) {
      state.localLoading = payload
    },
    setError (state, payload) {
      state.error = payload
    },
    clearError (state) {
      state.error = null
    }
  },
  actions: {
    setLoading ({commit}, payload) {
      commit('setLoading', payload)
    },
    setLocalLoading ({commit}, payload) {
      commit('setLocalLoading', payload)
    },
    setError ({commit}, payload) {
      commit('setError', payload)
    },
    clearError ({commit}) {
      commit('clearError')
    }
  },
  getters: {
    loading (state) {
      return state.loading
    },
    localLoading (state) {
      return state.localLoading
    },
    error (state) {
      switch (state.error) {
        case 'There is no user record corresponding to this identifier. The user may have been deleted.':
          state.error = 'Пользователь с таким email не найден.'
          break
      }
      return state.error
    }
  }
}
