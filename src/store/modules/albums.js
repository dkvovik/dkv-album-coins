import fb from 'firebase/app'
import 'firebase/firestore'
import 'firebase/storage'

class Album {
  constructor (title, description, userId, imageSrc = '') {
    this.title = title
    this.description = description
    this.userId = userId
    this.imageSrc = imageSrc
  }
}

export default {
  state: {
    albums: []
  },
  mutations: {
    addAlbum (state, payload) {
      state.albums.push(payload)
    },
    fetchAlbums (state, payload) {
      state.albums = payload
    },
    updateAlbum (state, payload) {
      let editedAlbum = state.albums.find(album => album.id === payload.id)
      editedAlbum.title = payload.title
      editedAlbum.description = payload.description
    },
    deleteAlbum (state, payload) {
      const indexToRemove = state.albums.findIndex(album => album.id === payload.id)
      state.albums.splice(indexToRemove, 1)
    }
  },
  actions: {
    async addAlbum ({commit, getters}, payload) {
      commit('clearError')
      commit('setLocalLoading', true)

      const image = payload.image

      try {
        const album = new Album(
          payload.title,
          payload.description,
          getters.getUser.id,
          payload.imageSrc
        )

        const timeInMs = Date.now()
        const imageExt = image.name.slice(image.name.lastIndexOf('.'))
        await fb.storage().ref().child(`${album.userId}/${timeInMs}${imageExt}`).put(image)
          .then(async snapshot => {
            const url = await snapshot.ref.getDownloadURL()
            album.imageSrc = url
          })

        const newAlbum = await fb.firestore().collection('users').doc(`${getters.getUser.id}`).collection(`albums`).add(JSON.parse(JSON.stringify(album)))
        commit('addAlbum', {...album, id: newAlbum.id})
        commit('setLocalLoading', false)
      } catch (error) {
        commit('setLocalLoading', false)
        commit('setError', error.message)
        throw error
      }
    },
    async fetchAlbums ({commit, getters}) {
      commit('clearError')
      commit('setLoading', true)

      let allAlbums = []
      try {
        const fetchAlbums =
          await fb.firestore().collection('users').doc(`${getters.getUser.id}`).collection('albums').get()
        fetchAlbums.forEach((doc) => {
          let album = doc.data()
          album['id'] = doc.id
          allAlbums.push(album)
        })
        commit('fetchAlbums', allAlbums)
        commit('setLoading', false)
      } catch (error) {
        commit('setLoading', false)
        commit('setError', error.message)
        throw error
      }
    },
    async updateAlbum ({commit, getters, dispatch}, payload) {
      commit('clearError')
      commit('setLocalLoading', true)

      try {
        await fb.firestore().collection('users').doc(`${getters.getUser.id}`).collection('albums').doc(payload.id).update({
          title: payload.title,
          description: payload.description
        })
        commit('updateAlbum', payload)
        commit('setLocalLoading', false)
      } catch (error) {
        commit('setLocalLoading', false)
        commit('setError', error.message)
        throw error
      }
    },
    async deleteAlbum ({commit, getters}, payload) {
      commit('clearError')
      commit('setLocalLoading', true)

      const imageRef = await fb.storage().refFromURL(payload.imageSrc)
      try {
        await fb.firestore().collection('users').doc(`${getters.getUser.id}`).collection('albums').doc(payload.id).delete()
        await imageRef.delete()
        commit('deleteAlbum', payload)
        commit('setLocalLoading', false)
      } catch (error) {
        commit('setLocalLoading', false)
        commit('setError', error.message)
        throw error
      }
    }
  },
  getters: {
    getAlbums (state) {
      return state.albums
    },
    getAlbumById (state) {
      return (id) => state.albums.find(album => {
        return album.id === id
      })
    },
    getAlbumNames (state) {
      let albumNames = []
      state.albums.map(album => { albumNames.push(album.title) })
      return albumNames
    }
  }
}
