import Vue from 'vue'
import VueRouter from 'vue-router'
import fb from 'firebase/app'
import 'firebase/auth'

import Dashboard from '@/components/Dashboard'
import Login from '@/components/auth/Login'
import Registration from '@/components/auth/Registration'
import Albums from '@/components/albums/Albums'
import AddAlbum from '@/components/albums/AddAlbum'
import Album from '@/components/albums/Album-detail'
import Coins from '@/components/coins/Coins'
import AddCoins from '@/components/coins/AddCoin'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {path: '', name: 'dashboard', component: Dashboard, meta: { requiresAuth: true }},
    {path: '/login', name: 'albums', component: Login},
    {path: '/registration', name: 'registration', component: Registration},
    {path: '/albums', component: Albums, meta: { requiresAuth: true }},
    {path: '/add-album', component: AddAlbum, meta: { requiresAuth: true }},
    {path: '/album/:id', props: true, name: 'album', component: Album, meta: { requiresAuth: true }},
    {path: '/coins', name: 'coins', component: Coins, meta: { requiresAuth: true }},
    {path: '/add-coin', name: 'addCoins', component: AddCoins, meta: { requiresAuth: true }}
  ],
  mode: 'history'
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const isAuthenticated = fb.auth().currentUser
  if (requiresAuth && !isAuthenticated) {
    next('/login?loginError=true')
  } else {
    next()
  }
})

export default router
