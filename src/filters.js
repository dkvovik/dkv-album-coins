import Vue from 'vue'

Vue.filter('nominal', function (value) {
  if (!value) return ''
  switch (value) {
    case 0.02: return '2 копейки'
    case 1: return '1 рубль'
    default: return value
  }
})
