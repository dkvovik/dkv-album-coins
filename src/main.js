import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'

import fb from 'firebase/app'
import 'firebase/auth'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import './style.css'

import './filters.js'

Vue.use(Vuetify)

fb.initializeApp({
  apiKey: 'AIzaSyC8ylESqABiAQAczuSHMjNT5gUamBrTAeo',
  authDomain: 'albumcoins-fa4ec.firebaseapp.com',
  databaseURL: 'https://albumcoins-fa4ec.firebaseio.com',
  projectId: 'albumcoins-fa4ec',
  storageBucket: 'albumcoins-fa4ec.appspot.com',
  messagingSenderId: '372408958108'
})
const firestore = fb.firestore()
const settings = {timestampsInSnapshots: true}
firestore.settings(settings)

Vue.config.productionTip = false

/* eslint-disable no-new */
const unsubscribe = fb.auth().onAuthStateChanged(
  (fbUser) => {
    new Vue({
      el: '#app',
      router,
      store,
      components: {App},
      created () {
        if (fbUser) {
          this.$store.dispatch('autoLoginUser', fbUser)
        }
        this.$store.dispatch('fetchAlbums')
      },
      template: '<App/>'
    })

    unsubscribe()
  })
